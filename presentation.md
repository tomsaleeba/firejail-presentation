% Firejail
% Tom Saleeba
% April 2022

---

### About me
- writes in high level langs (JS, etc)
- I use (daily drive) Arch BTW
- kernel n00b
- docker (user, not dev) experience

---

<!-- from https://firejail.wordpress.com/ -->
Firejail is a SUID sandbox program that reduces the risk of security breaches
by restricting the running environment of untrusted applications using Linux
namespaces, seccomp-bpf and Linux capabilities. It allows a process and all
its descendants to have their own private view of the globally shared kernel
resources, such as the network stack, process table, mount table.

---

### What is `firejail`?
- command line tool for Linux
- setuid binary
- sandboxes other processes
- aimed at desktop users
- magic comes from kernel

---

### How does it work
- firejail configures, kernel does heavy lifting
- no daemon
- linux namespaces: isolation (fs, net, kernel interface)
- Mandatory Access Control (MAC): control filesystem
- security filters: syscalls, user namespace, capabilities, etc
- networking: interfaces, shaping, firewall
- resource (cpu, ram, network) allocation

---

### About the project
- https://github.com/netblue30/firejail
- 4.2k stars
- https://firejail.wordpress.com/
- 8.3k commits
- started 2015
- ~99 commiters; 32 with > 10 commits

---

### About the project (cont'd)
- minimal dependencies
- written in C
- lightweight, low overhead
- 3.x kernel or newer
- GPLv2
- no business affiliation, no commercial goals

---

### What problems does it solve?
- executables you run can:
  - see all your stuff, and
  - act as you
- ransomware/supply-chain attacks mangle your files<br />
  (think `node-ipc` but worse)
- enforcing "principle of least privilege"
- bonus: cannot run more than one instance of a program

---

### How does it help me
- files
- syscalls
- networks
- X11
<!-- FIXME ipc -->

---

### How does it help me (files)
- control access to files and directories
  - are they visible inside the sandbox at all
  - visible but read-only
  - read-write

---

### How does it help me (syscalls)
- restrict allowed syscalls via seccomp-bpf
- BPF is proven tech
- violations logged via syslog

---

### How does it help me (networks)
- control networks
  - is there one at all
  - are other parts of the host visible
  - are other sandboxes visible
  - which hosts can be contacted
  - host allowlist can be auto-trained in first minute

---

### How does it help me (X11)
- X11 sandboxing
  - protect your clipboard
  - stop keyloggers from always recieving key strokes
- `xpra`: remote display server and client
- `xephyr`: run a separate window manager

---

### Comparison
- vs full-fat virtualisation:
  - near native speed, way faster
  - uses binaries from your system, no separate package system
- vs docker:
  - more granular control of resources
  - just for sandboxing, not distribution/repeatability

---

### How do I use it
- `firecfg` can configure for all known apps. Basically
  ```
  sudo ln -s $(which firejail) /usr/local/bin/<app>
  # update all `/usr/share/applications/*.desktop` files
  ```
- adhoc with command line params
  ```bash
  firejail [params...] <app>
  firejail --private --net=none something-dodgy
  ```

---

### How do I use it (cont'd)
- write your own profiles, and run with
  ```bash
  firejail --profile=/path/to/your.profile some-app
  ```
- extend built-in profiles with<br />`~/.config/firejail/<app>.local`

---

### How do I use it (cont'd)
- list your running jails with
  ```bash
  $ firejail --list
  1456:tom::/usr/bin/firejail /usr/bin/dropbox start
  125956:tom::/usr/bin/firejail /usr/bin/nvim package.json
  210794:tom::/usr/bin/firejail /usr/bin/yarn start
  238666:tom::/usr/bin/firejail /usr/bin/yarn serve
  240491:tom::/usr/bin/firejail /usr/bin/nvim .env
  258714:tom::/usr/bin/firejail /usr/bin/slack -s
  263137:tom::/usr/bin/firejail /usr/bin/flameshot
  267636:tom::/usr/bin/firejail /usr/bin/firefox
  272735:tom::/usr/bin/firejail /usr/bin/nvim presentation.md
  ```

---

<img src="./chromium-in-jail.png" style="height: 70vh;" />
<br />
<small>Chromium has limited view of `$HOME`</small>

---

### There's also a GUI
- gives you some interesting info
  - CPU usage
  - file browser
  - firewall details
- it's a bit clunky

---

<img src="./gui-proclist.png" style="height: 60vh;" />

---

<img src="./gui-prochome.png" style="height: 60vh;" />

---

### Risks of using `firejail`
- untrusted local user can use it to easily escalate privs
- you trust `firejail` to not get compromised
- you trust the pre-built profiles:
  - that they work, and
  - don't do anything malicious
- relies on `PATH`. Fully qualified shebang will evade a jail
- have to `firecfg` after installing new apps?

---

### Friction/pain
- default `nvim.profile` kills clipboard integration<br />
  ```bash
  # fix by enabling x11
  echo 'ignore x11' >> ~/.config/firejail/nvim.local
  ```
- "new tab" in terminal can't read current working dir
- using a `firejail`ed vim:
  ```bash
  # gives a 'noperm' error
  vim ~/.config/firejail/nvim.local

  # happily opens but edits the file in $HOME!!!
  cd ~/.config/firejail; vim nvim.local
  ```

---

### Friction/pain (cont'd)
- default `yarn` profile blocks `docker` commands
- default `yarn` can't recursively call itself (private tmp issue?)
- WidevineCdm plugin (DRM?) crashes in Firefox with default profile
- firefox "default browser" prompt; can't rename mount point
  <!-- https://github.com/netblue30/firejail/issues/1355 -->

---

<img src="./slow-ctrl-c.gif" style="width: 80vw;" />
<br />
<small>Small but noticeable lag sometimes</small>

---

### Pro-tip: files in sandbox
```bash
# list files in sandbox
firejail --ls=pid /some/dir

# copy a file to current dir
firejail --get=pid /some/file.txt

# copy file into sandbox
firejail --put=pid src.txt /full/path/to/dest.txt

# join a sandbox (depends on perms, haven't used this)
firejail --join=pid bash
```
<!-- https://github.com/netblue30/firejail/issues/3948 -->

---

### Pro-tip: disable for a command
```
function nofj {
  # runs a command without firejail
  PATH=/usr/bin:$PATH $*
}

# then use it
nofj some_command
```

---

### Summary
```bash
# install tool
sudo apt install firejail

# configure to sandbox known apps
sudo firecfg

# run stuff as you normally would
some_app

# run adhoc containers
firejail --private firefox
```

---

## TL;DR
Is it perfect? Tool yes, built-in profiles no.

Good enough to use now? Yes

Does it work? Yes

---

### Question?
<small>[source for pandoc presentation](https://gitlab.com/tomsaleeba/firejail-presentation)</small>

- bonus pain: [stack
  smashing](https://github.com/netblue30/firejail/issues/3350#issuecomment-612856055)
  protection killing me
  ```bash
  $ yarn lint --fix
  Error: too many environment variables: >= MAX_ENVS (256)

  # not many env vars
  $ env | wc -l
  91

  # ...but lots of shell vars
  $ set | wc -l
  305
  ```
<!-- https://github.com/netblue30/firejail/blob/81e12a45b7b1856bffc877d34266823207f8a5b7/src/firejail/main.c#L1051 -->
