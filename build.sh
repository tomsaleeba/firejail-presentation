#!/bin/sh
cd $(dirname "$0")
outDir=${1:-output}
mkdir -p $outDir/
pandoc -s -i -t revealjs presentation.md -o $outDir/index.html
cp *.png *.gif $outDir/
