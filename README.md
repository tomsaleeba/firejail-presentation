Source code to generate the presentation/slide show about the `firejail` tool.
See the generated presentation at:

https://tomsaleeba.gitlab.io/firejail-presentation/

`presentation.md` is the source for the presentation. See this for some links to
relevants details as comments (not visible in the final product).

### Building the presentation
```bash
./build.sh
# open ./output/index.html
```
