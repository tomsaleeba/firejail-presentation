# doesn't protect (fully hide) other root dirs
- it doesn't protect /zeta by default for me

# steam didn't work out-of-the-box
- `steam-runtime` didn't work well, I was seeing `steamwebhelper` do a core
  dump in syslog and most of the UI wasn't loading because it runs in a web
  browser

# troubleshooting perms tip
- you can troubleshoot perms issues by running the same profile but with a
  shell, so you can poke around
  ```
  firejail --profile=/etc/firejail/chromium.profile zsh
  ```

# too many env vars issue
- when running `yarn lint --fix` I got
  > Error: too many environment variables: >= MAX_ENVS (256)
  I don't have a heap of env vars, but I do have lots of shell vars
  ```
  $ set | wc -l
  305
  $ env | wc -l
  91
  ```
  https://github.com/netblue30/firejail/blob/81e12a45b7b1856bffc877d34266823207f8a5b7/src/firejail/main.c#L1051
  Seems like firejail passes on all shell vars, not just the "env" vars. The
  reason this limit exists is to combat [stack
  smashing](https://github.com/netblue30/firejail/issues/3350#issuecomment-612856055),
  so if that's not important to you, you could compile your own binary with a
  higher limit

# can customise firewall
```bash
$ firejail --net=eth0 --dns=9.9.9.9 firefox
```
> By default, if a network namespace is requested, Firejail installs a network
> filter customized for regular Internet browsing. It is a regular iptable filter,
> you can customize it, or you can bring in your own.

# FIXME
- can set env vars with `env` directive?
- whitelisting dirs in home works, but it doesn't for a mounted disk on /zeta.
  Not sure why
